<?php

interface IPedidoServico {
	
	public function __construct(IPedido $pedido);
	
	public function loadPedido(IPedido $pedido);
	
	public function salvar();

}