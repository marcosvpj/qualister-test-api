<?php

interface IPedido {
	
	public function __construct(INotaFiscal $notaFiscal);
	public function getPedidoItens();
	public function adicionaProduto(IProduto $produto, $unidades);
	public function finalizaPedido();
	
}