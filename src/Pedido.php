<?php

class Pedido implements IPedido{
	
	const ABERTO = 0;
	const SUCESSO = 1;

	public static $_status = array(
		self::ABERTO => 'Aberto',
		self::SUCESSO => 'Sucesso',
	);

	private $pedidoItens = array();
	private $status;

	private $notaFiscal;

	public function __construct(INotaFiscal $notaFiscal)
	{
		$this->status = self::ABERTO;
		$this->notaFiscal = $notaFiscal;
	}

	public function getPedidoItens()
	{
		return $this->pedidoItens;
	}

	public function adicionaProduto(IProduto $produto, $unidades)
	{
		for ($i=0; $i < $unidades; $i++) { 
			$this->pedidoItens[] = $produto;
			$produto->removeEstoque();
		}
	}

	public function finalizaPedido()
	{
		if(count($this->pedidoItens) <= 0)
			throw new Exception("Nenhum produto foi adicionado ao pedido", 20);

		if(!$this->notaFiscal->gerarNota($this))
			throw new Exception("Erro ao gerar nota fiscal", 20);

		// if(count($this->pedidoItens) > 0 && $this->notaFiscal->gerarNota())
		$this->status = self::SUCESSO;
		// else
		// 	throw new Exception("Nenhum produto foi adicionado ao pedido", 20);
		
		return true;
	}

	public function getStatus()
	{
		return $this->status;
	}

	public function getStatusMensagem()
	{
		return self::$_status[$this->status];
	}
}