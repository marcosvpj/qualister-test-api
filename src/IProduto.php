<?php

interface IProduto {
	
	public function __construct($dados);
	public function removeEstoque();
	public function getEstoque();

}