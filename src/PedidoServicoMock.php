<?php

class PedidoServicosMock implements IPedidoServico{

	private $pedido;

	public function __construct(IPedido $pedido)
	{
		$this->pedido = $pedido;
	}

	public function salvar()
	{
		// Salva o pedido em banco
		return true;
	}
	
}