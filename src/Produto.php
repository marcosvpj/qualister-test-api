<?php

class Produto implements IProduto{
	
	private $id;
	private $nome;
	private $valor;
	private $estoque;

	public function __construct($dados)
	{
		$this->id = $dados['id'];
		$this->nome = $dados['nome'];
		$this->estoque = $dados['estoque'];
		$this->valor = $dados['valor'];
	}

	public function removeEstoque()
	{
		if($this->estoque - 1 < 0)
			throw new Exception("Nao ha estoque disponivel", 20);
		else
			return $this->estoque--;
	}

	public function getEstoque()
	{
		return $this->estoque;
	}
}