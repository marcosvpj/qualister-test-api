<?php

class NotaFiscal implements INotaFiscal{

	private $gerar = false;

	public function __construct($gerar = false)
	{
		$this->gerar = $gerar;
	}
	
	public function gerarNota(IPedido $pedido)
	{
		return $this->gerar;
	}
}