<?php

interface INotaFiscal{
	
	public function gerarNota(IPedido $pedido);
}