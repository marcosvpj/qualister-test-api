<?php

include_once 'vendor/autoload.php';

class QuickLojaTest extends PHPUnit_Framework_TestCase{
	
	private $login = 'marcos.grupow';
	private $senha = 'grupow123';

	private $url;


	public function setUp()
	{
		$this->url = 'http://www.planned.by/quickloja';
	}

	public function testProcessoDeCadastrarPedido()
	{
		$driver = RemoteWebDriver::create(
			"http://localhost:4444/wd/hub",
			DesiredCapabilities::firefox()
		);
		$driver->manage()->window()->maximize();

		$driver->get($this->url);

		// Login
		$driver->findElement(WebDriverBy::cssSelector('#usuariologin[name="usuariologin"]'))->click()->clear()->sendKeys($this->login);
		$driver->findElement(WebDriverBy::cssSelector('#usuariosenha[name="usuariosenha"]'))->click()->clear()->sendKeys($this->senha);

		$driver->findElement(WebDriverBy::cssSelector('button[type="submit"]'))->click();

		$usuarioLogado = $driver->findElement(WebDriverBy::cssSelector('.navbar-text'))->getText();

		$this->assertContains('Entrou como Marcos', $usuarioLogado, 'Erro ao efetuar login');

		// Novo pedido
		$driver->findElement(WebDriverBy::cssSelector('a#novopedido'))->click();
		
		$novoPedido = $driver->findElement(WebDriverBy::xpath('/html/body/form/p'))->getText();
		$this->assertContains('Cadastre aqui seu pedido', $novoPedido, 'Erro ao abrir novo pedido');

		// Cadastrar pedido
	
		$driver->findElement(WebDriverBy::cssSelector('a#buscar'))->click();
		
		$modalClientes = $driver->findElement(WebDriverBy::cssSelector('.modal-header'))->getText();
		$this->assertContains('Pesquisar clientes', $modalClientes, 'Erro ao abrir modal de clientes');
		
		// Selecionar cliente
		$driver->findElement(WebDriverBy::linkText('Pedro - Pessoa Fisica'))->click();
		$clienteSelecionado = $driver->findElement(WebDriverBy::cssSelector('input#clientenome'))->getAttribute('value');

		$this->assertContains('Pedro - Pessoa Fisica', $clienteSelecionado);
		
		// Verifica cliente selecionado
		$emailCliente = $driver->findElement(WebDriverBy::cssSelector('#clienteemail'))->getText();
		$this->assertContains('pedro@test.com.br', $emailCliente);

		$select = $driver->findElement(WebDriverBy::cssSelector('#formaid'));
		$select = new WebDriverSelect($select);
		$select->selectByVisibleText('Pagamento à vista');

		$select = $driver->findElement(WebDriverBy::cssSelector('#pedidoparcelas'));
		$select = new WebDriverSelect($select);
		$select->selectByVisibleText('1');

		//Aba Dados de entrega
		$driver->findElement(WebDriverBy::linkText('Dados de entrega'))->click();

		$driver->findElement(WebDriverBy::cssSelector('input[name="pedidoendereco"]'))->sendKeys('Rua sem saida, 123');
		$driver->findElement(WebDriverBy::cssSelector('input[name="pedidobairro"]'))->sendKeys('centro');
		$driver->findElement(WebDriverBy::cssSelector('input[name="pedidocep"]'))->sendKeys('88330-000');
		$driver->findElement(WebDriverBy::cssSelector('input[name="pedidocidade"]'))->sendKeys('Balneario Camboriu');
		$estado = $driver->findElement(WebDriverBy::cssSelector('#pedidoestado'));
		$select = new WebDriverSelect($estado);
		$select->selectByVisibleText('Santa Catarina');
		
		$driver->findElement(WebDriverBy::linkText('Itens do pedido'))->click();
		$driver->findElement(WebDriverBy::cssSelector('#produtonome'))->sendKeys('t');
		
		$driver->wait(10,500)->until(
			WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector(".typeahead > li:nth-child(1)"))
		);
		$driver->findElement(WebDriverBy::cssSelector('.typeahead > li:nth-child(1)'))->click();
		
		$driver->findElement(WebDriverBy::cssSelector('#produtoquantidade'))->sendKeys('5');
		
		$driver->findElement(WebDriverBy::cssSelector('input#adicionar'))->click();
		// echo $driver->findElement(WebDriverBy::cssSelector('.listaItens'))->getText();
		
		$driver->findElement(WebDriverBy::cssSelector('button[type="submit"]'))->click();
		// Tenis Puma Axis 2 Branco
		//enis Puma Axis 2 Branco

		$sucesso = $driver->findElement(WebDriverBy::cssSelector('.alert.alert-success'))->getText();
		$this->assertEquals('Sucesso ao inserir o pedido', $sucesso, 'Nao inseriu o pedido');
		
		$driver->findElement(WebDriverBy::linkText('Movimentações'))->click();

		$valorPedido = $driver->findElement(WebDriverBy::cssSelector('.table > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(6)'))->getText();
		$this->assertEquals('R$ 371,25', $valorPedido, 'Valor do ultimo pedido não bate');
		
		$driver->findElement(WebDriverBy::linkText('Sair'))->click();
		
		$sucessoLogout = $driver->findElement(WebDriverBy::cssSelector('.alert.alert-success'))->getText();
		$this->assertEquals('Sessão encerrada com sucesso', $sucessoLogout, 'Nao saiu do sistema');

		$driver->quit();
	}

}