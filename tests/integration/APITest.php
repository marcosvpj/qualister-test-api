<?php

include_once 'vendor/autoload.php';

class APITest extends PHPUnit_Framework_TestCase {
	
	private $client;
	private $uri;

	public function setUp()
	{
		$this->client =  new GuzzleHttp\Client();
		// $this->uri =  'http://192.168.2.164:8888';
		$this->uri =  'http://localhost:8888';
	}

	/**
	* @test
	* @author Marcos V
	**/
	public function testAcessoRootEntaoReceber200SucessoECodigo()
	{
		// $this->marktestIncomplete('Teste nao desenvolvido');

		// Arrange

		// Act
		$resposta = $this->client->get($this->uri.'/', ['auth' => ['phptesting','123']]);
		$body = json_decode($resposta->getBody(), true);

		// Assert
		$this->assertEquals(200, $resposta->getStatusCode(), 'Codigo de resposta http errado');
		$this->assertEquals('application/json', $resposta->getHeader('content-type')[0], 'Content type errado');

		$this->assertEquals('sucesso', $body['status'], 'Status errado');
		$this->assertEquals('Bem vindos a API Qualister', $body['message'], 'Mensagem errado');
		$this->assertEquals('gnitsetphp', $body['data']['codigo']);
	}


	/**
	* @test
	* @author Marcos V
	**/
	public function testAcessoListaDePedidosReceber200ListaVazia()
	{
		// Arrange
		
		// Act
		$resposta = $this->client->get($this->uri.'/pedido', ['auth' => ['phptesting','123']]);
		$body = json_decode($resposta->getBody(), true);
		
		// Assert
		$this->assertEquals(200, $resposta->getStatusCode(), 'Codigo de resposta http errado');
		$this->assertEquals('application/json', $resposta->getHeader('content-type')[0], 'Content type errado');

		$this->assertEquals('sucesso', $body['status'], 'Status errado');
		$this->assertEquals('A lista esta vazia', $body['message'], 'Mensagem errado');
		$this->assertCount(0, $body['data'], 'Dados retornados errado');
	}

	/**
	* @test
	* @author Marcos V
	**/
	public function testChamarPedido1EReceber200SucessoNomeECodigoCliente()
	{
		$resposta = $this->client->get($this->uri.'/pedido/1?clientenome=Fulano', ['auth' => ['phptesting','123']]);
		$body = json_decode($resposta->getBody(), true);
		
		// Assert
		$this->assertEquals(200, $resposta->getStatusCode(), 'Codigo de resposta http errado');
		$this->assertEquals('application/json', $resposta->getHeader('content-type')[0], 'Content type errado');

		$this->assertEquals('sucesso', $body['status'], 'Status errado');
		$this->assertEquals('Seu codigo e 1', $body['message'], 'Mensagem errado');
		$this->assertEquals('Fulano', $body['data']['clientenome'], 'Dados retornados errado');
	}

	/**
	* @test
	* @author Marcos V
	* @dataProvider dataAdicionarItemAoPedidoERetornarSucesso
	**/
	public function testAdicionarItemAoPedidoERetornarSucesso($produtoId, $produtoNome, $produtoEstoque, $produtoValor, $produtoQuantidade, $mensagemRespota)
	{
		// Act
		$resposta = $this->client->post($this->uri."/pedido", ['auth' => ['phptesting', '123'],
			'form_params' => [
						'produtoid' => $produtoId, // 1,
						'produtonome' => $produtoNome, // 'PlayStation',
						'produtoestoque' => $produtoEstoque, // 50,
						'produtovalor' => $produtoValor, // 59.90,
						'quantidade' => $produtoQuantidade, // 10
				]
			]
		);

		$body = json_decode($resposta->getBody(), true);

		$this->assertEquals($mensagemRespota, $body['message'], 'message');
		// $this->assertEquals('Sucesso', $body['message'], 'message');
	}

	public function dataAdicionarItemAoPedidoERetornarSucesso()
	{
		return array_map('str_getcsv', file('tests/integration/ddt/dataAdicionarItemAoPedidoERetornarSucesso.csv'));
	}

	/**
	* @author Marcos V
	**/
	public function testValidarUmSchemaJson()
	{
		// Arrange
		$resposta = $this->client->get($this->uri . "/",['auth' => ['phptesting', '123']]);
	
		// Act
		$validator = new \Json\Validator('tests/integration/schema/schema.json');
	
		// Assert
		$validator->validate(json_decode($resposta->getBody()));
	}
}