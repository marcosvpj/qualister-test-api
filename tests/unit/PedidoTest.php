<?php

include 'vendor/autoload.php';

class PedidoTest extends PHPUnit_Framework_Testcase{

	private $pedido;

	public static function setUpBeforeClass()
	{
		echo 'Iniciando os testes' . "\n";
	}
	
	public static function tearDownAfterClass()
	{
		echo "\n".'Finalizando os testes' . "\n";
	}

	protected function setUp()
	{
		// $notaFiscal = new NotaFiscal();
		$notaFiscal = $this->getMockBuilder('INotaFiscal')->getMock();
		$notaFiscal->method('gerarNota')->with($this->isInstanceOf('IPedido'))->willReturn(true);
		
		$this->pedido = new Pedido($notaFiscal);
	}

	/**
	* @test
	* @author Marcos V
	* @small
	* @covers Pedido
	* @covers Pedido::getPedidoItens
	**/
	public function testListaDePedidosDeveConter0Itens(){
		// Arrange
		

		// Act
		$pedidoItens = $this->pedido->getPedidoItens();
		
		// Assert
		$this->assertCount(0, $pedidoItens);

	}

	/**
	* @test
	* @group Importantes
	* @author Marcos V
	* @medium
	* @covers Produto
	* @covers Produto::getEstoque
	* @covers Pedido
	* @covers Pedido::adicionaProduto
	* @covers Pedido::getPedidoItens
	**/
	public function testAdicionar2UnidadesDeProdutoAoPedidoProdutoAdicionado()
	{
		// Arrange
		$produtoBanana = new Produto(array(
			'id' => 1,
			'nome' => 'Banana',
			'estoque' => 50,
			'valor' => 2.99,
		));


		$produtoManga = new Produto(array(
			'id' => 2,
			'nome' => 'Manga',
			'estoque' => 3,
			'valor' => 4.99,
		));

		// Act
		$this->pedido->adicionaProduto($produtoBanana, 1);
		$this->pedido->adicionaProduto($produtoManga, 1);

		$pedidoItens = $this->pedido->getPedidoItens();

		// Assert
		$this->assertCount(2, $pedidoItens);

		$this->assertEquals($produtoBanana, $pedidoItens[0]);
		$this->assertEquals($produtoManga, $pedidoItens[1]);

		$this->assertEquals(49, $produtoBanana->getEstoque());
		$this->assertEquals(2, $produtoManga->getEstoque());
	}

	/**
	* @test
	* @author Marcos V
	* @group excecao
	* @expectedException 		Exception
	* @expectedExceptionCode 	20
	* @expectedExceptionMessage	Nao ha estoque disponivel
	* @covers Pedido
	* @covers Pedido::adicionaProduto
	* @dataProvider dataErroAoAdicionarMaisProdtosQueDisponivelEmEstoque
	**/
	public function testErroAoAdicionarMaisProdtosQueDisponivelEmEstoque($id, $nome, $estoque, $valor, $unidades)
	{
		// Arrange
		$produtoBanana = new Produto(array(
			'id' => $id,
			'nome' => $nome,
			'estoque' => $estoque,
			'valor' => $valor,
		));
		// $produtoBanana = new Produto(array(
		// 	'id' => 1,
		// 	'nome' => 'Banana',
		// 	'estoque' => 5,
		// 	'valor' => 2.99,
		// ));
		
		// Action
		$this->pedido->adicionaProduto($produtoBanana, $unidades);
		// $this->pedido->adicionaProduto($produtoBanana, 10);
	}

	public function dataErroAoAdicionarMaisProdtosQueDisponivelEmEstoque(){
		return array_map('str_getcsv', file('tests/unit/ddt/dataErroAoAdicionarMaisProdtosQueDisponivelEmEstoque.csv'));
	}
	
	/**
	* @test
	* @author Marcos V
	* @covers Pedido
	* @covers Pedido::adicionaProduto
	* @covers Pedido::getStatus
	* @covers PedidoServicos
	* @covers PedidoServicos::salvar
	* @covers Produto
	* @dataProvider dataAdicionarProdutosComMensagemDeSucesso
	**/
	public function testAdicionarProdutosComMensagemDeSucesso($produto1, $produto2, $unidades1, $unidades2, $statusFinal)
	{
		// Arrange
		$produtoBanana = new Produto($produto1);
		$produtoManga = new Produto($produto2);
		
		// $produtoBanana = new Produto(array(
		// 	'id' => 1,
		// 	'nome' => 'Banana',
		// 	'estoque' => 5,
		// 	'valor' => 2.99,
		// ));
		// $produtoManga = new Produto(array(
		// 	'id' => 2,
		// 	'nome' => 'Manga',
		// 	'estoque' => 10,
		// 	'valor' => 3.99,
		// ));

		// Action
		$this->pedido->adicionaProduto($produtoBanana, $unidades1);
		$this->pedido->adicionaProduto($produtoManga, $unidades2);
		// $this->pedido->adicionaProduto($produtoBanana, 1);
		// $this->pedido->adicionaProduto($produtoManga, 1);

		$this->pedido->finalizaPedido();

		// $pedidoServico = new PedidoServicos($this->pedido);
		$pedidoServico = $this->getMockBuilder('IPedidoServico')->getMock();
		$pedidoServico->method('salvar')
						->willReturn(true);

		$resultadoSalvar = $pedidoServico->salvar();

		$status = $this->pedido->getStatus();
		$statusMensagem = $this->pedido->getStatusMensagem();

		// Assert
		$this->assertTrue($resultadoSalvar);

		$this->assertEquals(Pedido::SUCESSO, $status);
		$this->assertEquals(Pedido::$_status[Pedido::SUCESSO], $statusMensagem);
		$this->assertEquals($statusFinal, $statusMensagem);

		// $this->assertEquals('Sucesso', $statusMensagem);
	}

	public function dataAdicionarProdutosComMensagemDeSucesso()
	{
		return array(
			array(	array('id' => 1, 'nome' => 'Banana', 'estoque' => 5, 'valor' => 2.99), 
					array('id' => 2, 'nome' => 'Manga', 'estoque' => 10, 'valor' => 3.99), 
					1, 
					1, 
					'Sucesso'
			),
			array(	array('id' => 3, 'nome' => 'PS', 'estoque' => 7, 'valor' => 1999.99), 
					array('id' => 4, 'nome' => 'GB', 'estoque' => 32, 'valor' => 150.00), 
					1,
					1,
					'Sucesso'
			),
			array(	array('id' => 5, 'nome' => 'Jack Daniels', 'estoque' => 12, 'valor' => 120.00), 
					array('id' => 6, 'nome' => 'Jim Bean', 'estoque' => 12, 'valor' => 90.00), 
					2,
					2,
					'Sucesso'
			),
			array(	array('id' => 7, 'nome' => 'Leffe Blonde', 'estoque' => 32, 'valor' => 9.99), 
					array('id' => 8, 'nome' => 'Colorado Appia', 'estoque' => 22, 'valor' => 14.89), 
					2,
					3,
					'Sucesso'
			),
		);
	}

	/**
	* @test
	* @author Marcos V
	* @group excecao
	* @expectedException 		Exception
	* @expectedExceptionCode 	20
	* @expectedExceptionMessage	Nenhum produto foi adicionado ao pedido
	* @covers Pedido
	* @covers Pedido::finalizaPedido
	**/
	public function testErroFinalizaPedidoVazio()
	{
		// Action
		$this->pedido->finalizaPedido();
	}

	/**
	* @test
	* @author Marcos V
	* @group mock
	* @covers Produto
	* @covers Produto::getEstoque
	**/
	public function testPedidoRetornaSucesso()
	{
		// Arrange
		$produto = new Produto(array('id' => 1,'nome' => 'Banana','estoque' => 5,'valor' => 2.99));

		// $produto = $this->getMockBuilder('IProduto')
		// 				->setConstructorArgs(array(array('id' => 1,'nome' => 'Banana','estoque' => 5,'valor' => 2.99)))
		// 				->getMock();
		
		// $produto->method('getEstoque')->willReturn(4);

		// $pedidoServico = new PedidoServicos($this->pedido);
		$pedidoServico = $this->getMockBuilder('IPedidoServico')->getMock();

		$pedidoServico->expects($this->once())
						->method('loadPedido')
						->with($this->isInstanceOf('IPedido'))
						->willReturn(true);

		$pedidoServico->method('salvar')
						->willReturn(true);

		// Action
		$this->pedido->adicionaProduto($produto, 1);
		$this->pedido->finalizaPedido();

		$resultadoLoad = $pedidoServico->loadPedido($this->pedido);
		$resultadoSalvar = $pedidoServico->salvar();

		$status = $this->pedido->getStatusMensagem();

		$estoque = $produto->getEstoque();

		// Assert
		$this->assertEquals('Sucesso', $status);
		$this->assertEquals(4, $estoque);

		$this->assertTrue($resultadoLoad, 'Erro no load');
		$this->assertTrue($resultadoSalvar, 'Erro no salvar');
	}

	public function tearDown()
	{
		unset($this->pedido);
	}
}

?>
