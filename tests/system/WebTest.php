<?php

include_once 'vendor/autoload.php';

class WebTest extends PHPUnit_Framework_TestCase{
	
	private $uri;

	public function setUp()
	{
		$this->uri = "http://localhost:8080";
	}

	/**
	* @author Marcos V
	**/
	public function testAcessarPaginaInicialEVerMensagemBemVindo()
	{
		$driver = RemoteWebDriver::create(
			"http://localhost:4444/wd/hub",
			DesiredCapabilities::firefox()
		);

		$driver->manage()->window()->maximize();
		$driver->get($this->uri);
		$driver->quit();
	}

	/**
	* @author Marcos V
	* @dataProvider dataNavegacaoEntrePaginaInicialEListageDeItens
	**/
	public function testNavegacaoEntrePaginaInicialEListageDeItens($navegador)
	{
		$driver = RemoteWebDriver::create(
			"http://localhost:4444/wd/hub",
			$navegador
		);
		$driver->manage()->window()->maximize();
		$driver->get($this->uri);
		
		$driver->navigate()->to($this->uri . '/listaritens.php');
		
		$titulo = $driver->getTitle();

		$this->assertEquals('Starter Template - Materialize', $titulo, 'Tituo nao bate');

		$driver->navigate()->refresh();
		$driver->navigate()->back();
		$driver->navigate()->forward();

		$driver->quit();
	}

	public function dataNavegacaoEntrePaginaInicialEListageDeItens()
	{
		return array(
			array(DesiredCapabilities::firefox()),
			array(DesiredCapabilities::chrome()),
		);
	}

	/**
	* @author Marcos V
	* @dataProvider AcessarPaginaInicialERetornarTituloETextoLogoETextoLaranja
 	**/
	public function testAcessarPaginaInicialERetornarTituloETextoLogoETextoLaranja($navegador)
	{
		$driver = RemoteWebDriver::create(
			"http://localhost:4444/wd/hub",
			// "http://192.168.2.147:4444/wd/hub",
			$navegador
		);
		$driver->manage()->window()->maximize();

		// $driver->get('http://localhost/projects/qualister-php-testing/web');
		$driver->get($this->uri);
		
		$titulo = $driver->getTitle();
		
		$textoLogo = $driver->findElement(WebDriverBy::id("logo-container"))->getText();
		$textoLaranja = $driver->findElement(WebDriverBy::cssSelector(".orange-text"))->getText();
		$tagBotaoListarItens = $driver->findElement(WebDriverBy::cssSelector("#listaritens"))->getTagName();

		$driver->quit();

		// echo "\n";
		// echo "\n Navegador: \t\t\t" . $navegador->getBrowserName();
		// echo "\n Titulo: \t\t\t" . $titulo;
		// echo "\n Texto do logo: \t\t" . $textoLogo;
		// echo "\n Texto laranja: \t\t" . $textoLaranja;
		// echo "\n Tag do botao Listar Itens: \t" . $tagBotaoListarItens;
		// echo "\n";
	}


	public function AcessarPaginaInicialERetornarTituloETextoLogoETextoLaranja()
	{

		return array(
			array(DesiredCapabilities::firefox()),
			array(DesiredCapabilities::chrome()),
		);
	}

	/**
	* @author Marcos V
	* @dataProvider AcessarPaginaInicialERetornarTituloETextoLogoETextoLaranja
 	**/
	public function testAcessarPaginaInicialEVerificarCopyright($navegador)
	{
		$driver = RemoteWebDriver::create(
			"http://localhost:4444/wd/hub",
			$navegador
		);
		$driver->manage()->window()->maximize();

		$driver->get($this->uri);
		
		$copyright = $driver->findElement(WebDriverBy::cssSelector('.footer-copyright'))->getText();
		$tag = $driver->findElement(WebDriverBy::cssSelector('.no-pad-bot'))->getTagName();
		
		$this->assertEquals('Made by Materialize', $copyright, 'Erro no copyright');
		$this->assertEquals('div', $tag, 'Tag do no-pad-bot diferente de div');

		$driver->quit();
	}

	/**
	* @author Marcos V
	**/
	public function testAcessaPaginaInicialEVerificarTextoBanner()
	{
		$driver = RemoteWebDriver::create(
			"http://localhost:4444/wd/hub",
			DesiredCapabilities::firefox()
		);
		$driver->manage()->window()->maximize();

		$driver->get($this->uri);
		
		$textBanner = $driver->findElement(WebDriverBy::xpath('//*[@id="index-banner"]'))->getText();
		$hrefMenu = $driver->findElement(WebDriverBy::xpath('//li[1]/a'))->getAttribute('href');
		$textWhiteText = $driver->findElement(WebDriverBy::xpath('//*[@class="white-text"]'))->getText();
		
		$this->assertEquals('Bem vindos a API Qualister', $textBanner, 'Erro no texto do banner');
		$this->assertContains('listaritens.php', $hrefMenu, 'Erro no href do menu');
		$this->assertEquals('Company Bio', $textWhiteText, 'Erro no text do white-text');

		$driver->quit();	
	}

	/**
	* @author Marcos V
	**/
	public function testAcessaPaginaInicialEVerificarMensageDeLogadoAoAcessarMeusPedidos()
	{
		$driver = RemoteWebDriver::create(
			"http://localhost:4444/wd/hub",
			DesiredCapabilities::firefox()
		);
		$driver->manage()->window()->maximize();

		$driver->get($this->uri);

		$driver->findElement(WebDriverBy::cssSelector('#meuspedidos'))->click();
		
		$textAlert = $driver->switchTo()->alert()->accept()->getText();
		$driver->switchTo()->alert()->accept();
		
		$this->assertEquals('Faça login antes', $textAlert, 'Erro no texto do alert');

		$driver->quit();	
	}


	/**
	* @author Marcos V
	* @group exe6
	* @dataProvider dataAcessaNovoPedidoCadastraPedidoERecebeMensagemDeSucesso 
	**/
	public function testAcessaNovoPedidoCadastraPedidoERecebeMensagemDeSucesso($navegador)
	{
		// Arrange
		$driver = RemoteWebDriver::create(
			"http://localhost:4444/wd/hub",
			// DesiredCapabilities::firefox()
			$navegador
		);
		$driver->manage()->window()->maximize();

		// Action
		$driver->get($this->uri);

		$driver->findElement(WebDriverBy::cssSelector('#novopedido'))->click();

		$driver->findElement(WebDriverBy::cssSelector('#id[name="id"]'))->click()->clear()->sendKeys('1');
		$driver->findElement(WebDriverBy::cssSelector('#estoque[name="estoque"]'))->click()->clear()->sendKeys('35');
		$driver->findElement(WebDriverBy::cssSelector('#valor[name="valor"]'))->click()->clear()->sendKeys('2.99');

		$select = $driver->findElement(WebDriverBy::cssSelector('#produto[name="produto"]'));
		$select = new WebDriverSelect($select);
		$select->selectByVisibleText('Firefox');

		$driver->findElement(WebDriverBy::cssSelector('label[for="quantidade5"]'))->click();
		
		$driver->findElement(WebDriverBy::cssSelector('button[name="action"]'))->click();

		$textAlert = $driver->switchTo()->alert()->getText();
		$driver->switchTo()->alert()->accept();
		
		// Assert
		$this->assertEquals('Sucesso', $textAlert, 'Erro no texto do alert');

		$driver->quit();
	}

	public function dataAcessaNovoPedidoCadastraPedidoERecebeMensagemDeSucesso()
	{
		return array(
			array(DesiredCapabilities::firefox()),
			array(DesiredCapabilities::chrome()),
		);
	}

	/**
	* @author Marcos V
	* @group exe7
	**/

	public function testAcessarPaginaInicialEAguardarMensagemSecreta()
	{
		$driver = RemoteWebDriver::create(
			"http://localhost:4444/wd/hub",
			DesiredCapabilities::firefox()
		);
		$driver->manage()->window()->maximize();

		$driver->manage()->timeouts()->implicitlyWait(2);
		$driver->get($this->uri);

		// wait(aguardar_até_segundos, verificar_a_cada_milisegundos)
		$driver->wait(10,500)->until(
			WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::id("mensagem-magica"))
		);

		$textoMensagem = $driver->findElement(WebDriverBy::cssSelector('#mensagem-magica'))->getText();

		
		$this->assertEquals('Você é paciente!', $textoMensagem, 'Erro no texto do alert');

		$driver->quit();
	}
}