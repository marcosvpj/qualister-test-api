<?php
require '../vendor/autoload.php';

$app = new \Slim\Slim();

$valid_passwords = array ("phptesting" => "123");
$valid_users = array_keys($valid_passwords);
$user = $_SERVER['PHP_AUTH_USER'];
$pass = $_SERVER['PHP_AUTH_PW'];
$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);
if (!$validated) {
	header('WWW-Authenticate: Basic realm="My Realm"');
	header('HTTP/1.0 401 Unauthorized');
	die ("Not authorized");
	exit();
}

$app->get('/', function () {
	$resposta = array(
		"status" => "sucesso",
		"message" => "Bem vindos a API Qualister",
		"data" => array("codigo" => "gnitsetphp")
		);

	header("Content-Type: application/json");
	header('HTTP/1.0 200 OK');
	echo json_encode($resposta);
	exit();
});

$app->get('/pedido', function() {
	$notaFiscal = new NotaFiscal();
	$pedido = new Pedido($notaFiscal);

	$itens = $pedido->getPedidoItens();

	$resposta = array(
		'status' => 'sucesso',
		'message' => 'A lista esta vazia',
		'data' => $itens
	);

	header("Content-Type: application/json");
	header('HTTP/1.0 200 OK');
	echo json_encode($resposta);
	exit();
});

$app->get('/pedido/:id', function ($id) use ($app) {
	$clientenome = $app->request()->get("clientenome");
	$resposta = array(
		"status" => "sucesso",
		"message" => "Seu codigo e $id",
		"data" => array("clientenome" => $clientenome)
	);

	header("Content-Type: application/json");
	header('HTTP/1.0 200 OK');
	echo json_encode($resposta);
	exit();
});

$app->get('/:controllerName/:actionName', function ($controllerName, $actionName) use($app) {
	$controllerName = ucfirst(strtolower($controllerName));
	$actionName = ucfirst(strtolower($actionName));
	
	if(is_file('controller/'.$controllerName.'.php')){
		include_once 'controller/'.$controllerName.'.php';
		$controller = new $controllerName;

		// if($controller instanceof $controllerName){
			$action = 'action' . $actionName;
			call_user_func( array( $controller, $action ) );
		// }
	} else {
		echo 'erro 404';
	}
	
	exit(); 
});

$app->post('/pedido', function () use ($app) {
	$produtoid = $app->request()->post("produtoid");
	$produtonome = $app->request()->post("produtonome");
	$produtoestoque = $app->request()->post("produtoestoque");
	$produtovalor = $app->request()->post("produtovalor");
	$quantidade = $app->request()->post("quantidade");

	$notaFiscal = new NotaFiscal(true);
	$pedido = new Pedido($notaFiscal);
	$produto = new Produto(array('id'=>$produtoid,'nome'=>$produtonome,'estoque'=>$produtoestoque,'valor'=>$produtovalor));

	$pedido->adicionaProduto($produto, $quantidade);
	$pedidoservicos = new PedidoServicos($pedido);

	// echo $pedidoservicos->salvar($pedido);
	$pedido->finalizaPedido();
	$pedidoservicos->salvar($pedido);
	
	$resposta = array(
		"status" => "successo",
		"message" => $pedido->getStatusMensagem(),
		"data" => array()
	);

	header("Content-Type: application/json");
	header('HTTP/1.0 200 OK');
	echo json_encode($resposta);
	exit();
});

$app->run();
